# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.2.3] - 2021-09-01
### Fixed
* [BUGFIX] Refactor ESLint and Prettier steps to include `--no-error-on-unmatched-pattern` argument instead of creating dummy JS file.
* Fixed directory name entry in .gitignore
* Fixed package name to correct one in package.json
### Added
* Added missing reference URLs to CHANGELOG.md & fixed existing

[0.2.3]: https://bitbucket.org/vismadc/internal-pipeline-boilerplate-module/branches/compare/0.2.2%0D0.2.1


## [0.2.2] - 2021-09-01
### Fixed
* [BUGFIX] #EURO-223 Do not fail ESLint step when there are no applicable files in src directory.

[0.2.2]: https://bitbucket.org/vismadc/internal-pipeline-boilerplate-module/branches/compare/0.2.2%0D0.2.1


## [0.2.1] - 2021-09-01
### Changed
* [BUGFIX] #EURO-223 Do not fail Prettier step when there are no applicable files in src directory.

[0.2.1]: https://bitbucket.org/vismadc/internal-pipeline-boilerplate-module/branches/compare/0.2.1%0D0.2.0


## [0.2.0] - 2021-05-17
### Changed
* [FEATURE] #RD-279 Snyk security checks for PHP and NPM

[0.2.0]: https://bitbucket.org/vismadc/internal-pipeline-boilerplate-module/branches/compare/0.2.0%0D0.1.1


## [0.1.1] - 2021-05-17
### Changed
* The module was changed


## [0.1.0] - 2021-05-17
### Added
* The module was added.

[0.1.1]: https://bitbucket.org/vismadc/internal-pipeline-boilerplate-module/branches/compare/0.1.1%0D0.1.0
[0.1.0]: https://bitbucket.org/vismadc/internal-pipeline-boilerplate-module/src/0.1.0/
