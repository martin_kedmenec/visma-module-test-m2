<?php

namespace Unit1\Test\MagentoU;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Checkout\Model\Session;
use Unit1\Test\Api\ProductRepositoryInterface as Wtf;

class Test
{
    private $justAParameter;

    private $data;

    private $unit1ProductRepository;

    public function __construct(
        ProductRepositoryInterface $productRepositoryInterface,
        ProductFactory $productFactory,
        Session $session,
        Wtf $unit1ProductRepository,
        $justAParameter = false,
        array $data = []
    ) {
        $this->justAParameter = $justAParameter;
        $this->data = $data;
        $this->unit1ProductRepository = $unit1ProductRepository;
    }
}
